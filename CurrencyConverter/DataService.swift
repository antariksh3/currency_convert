//
//  DataService.swift
//  CurrencyConverter
//
//  Created by JASI on 14/11/19.
//  Copyright © 2019 lambton. All rights reserved.
//

import Foundation

class DataService {
    
    static let instance = DataService()
    
    var currencies = [ Currency(name: CurrencyEnum.USD.rawValue , img: "us_flag_new.png" ),
                        Currency(name: CurrencyEnum.CDN.rawValue , img: "canada_flag_new.png" ),
                        Currency(name: CurrencyEnum.YUAN.rawValue , img: "china_flag.png" ),
                        Currency(name: CurrencyEnum.INR.rawValue , img: "india_flag.png" ),
                        Currency(name: CurrencyEnum.EURO.rawValue , img: "eu_flag.png" )]
    
    
    func getCurrencies() -> [Currency] {
        return currencies
    }
    
}
