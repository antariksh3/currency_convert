//
//  CurrencyEnum.swift
//  CurrencyConverter
//
//  Created by JASI on 13/11/19.
//  Copyright © 2019 lambton. All rights reserved.
//

import Foundation

public enum CurrencyEnum : String {
    case USD , CDN , YUAN , INR , EURO
}
