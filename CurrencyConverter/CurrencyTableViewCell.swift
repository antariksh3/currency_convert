//
//  CurrencyTableViewCell.swift
//  CurrencyConverter
//
//  Created by JASI on 14/11/19.
//  Copyright © 2019 lambton. All rights reserved.
//

import UIKit

class CurrencyTableViewCell: UITableViewCell {

    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func updateView(currency : Currency) {
        img.image = UIImage.init(named: currency.currencyImage)
        label.text = currency.currencyName
    }

    
}
