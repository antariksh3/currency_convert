//
//  Session.swift
//  CurrencyConverter
//
//  Created by JASI on 13/11/19.
//  Copyright © 2019 lambton. All rights reserved.
//

import Foundation

public class CustomSession {
    var fromCurrency : String = CurrencyEnum.USD.rawValue
    var toCurrency : String = CurrencyEnum.CDN.rawValue
    var isCustom : Bool = false
    var exchangeRate = 1.33
}
