//
//  Currency.swift
//  CurrencyConverter
//
//  Created by JASI on 14/11/19.
//  Copyright © 2019 lambton. All rights reserved.
//

import Foundation
import UIKit

class Currency {
    
    var currencyName : String
    var currencyImage : String
    
    init( name : String , img : String ) {
        self.currencyName = name
        self.currencyImage = img
    }
}
