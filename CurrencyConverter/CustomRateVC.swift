//
//  CustomRateVC.swift
//  CurrencyConverter
//
//  Created by JASI on 17/11/19.
//  Copyright © 2019 lambton. All rights reserved.
//

import Foundation
import UIKit

class CustomRateVC : UIViewController {
    
    @IBOutlet weak var editTxt: UITextField!
    
    override func viewDidLoad() {
        print("CustomRateVC: view did laod")
        var vc = ViewController()
        var ses = vc.getCustomSession()
        print(ses.exchangeRate)
        editTxt.text = String(ses.exchangeRate)
    }
    
    @IBAction func okPressed(_ sender: Any) {
        print("Saving", editTxt.text! )
        var vc = ViewController()
        var ses = vc.getCustomSession()
        ses.exchangeRate = (editTxt.text! as NSString).doubleValue
        ses.isCustom = true
        self.navigationController?.popViewController(animated: true)

        
    }
    
}
