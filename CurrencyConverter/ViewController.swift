//
//  ViewController.swift
//  CurrencyConverter
//
//  Created by JASI on 13/11/19.
//  Copyright © 2019 lambton. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //initialize with default values
    var fromCurr : String = CurrencyEnum.USD.rawValue
    var toCurr : String = CurrencyEnum.CDN.rawValue
    var isCustom : Bool = false
    var exchRate : Double = 1.33
    
    static var session : MyCustomSession = MyCustomSession()
    
    @IBOutlet weak var inputTxt: UITextField!
    @IBOutlet weak var stackExchange: UIStackView!
    @IBOutlet weak var btnConvert: UIButton!
    @IBOutlet weak var fromCurrencyImg: UIImageView!
    @IBOutlet weak var fromCurrencyLb: UILabel!
    @IBOutlet weak var toCurrencyImg: UIImageView!
    @IBOutlet weak var toCurrencyLb: UILabel!
    @IBOutlet weak var defaultRateLb: UILabel!
    @IBOutlet weak var resultLb: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        print("ViewController: View Did Load")
        loadDataFromSession()
         self.hideKeyboardOnBackgroundTap()
        //loadImagesAndText()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("View will appear called")
        loadDataFromSession()
        resultLb.text = "Click on Convert to view Result"
        var ses = getCustomSession()
        if ( ses.fromCurrency == ses.toCurrency ) {
            stackExchange.isHidden = true
            btnConvert.isHidden = true
            resultLb.text = "Both Currencies Are Same"
        }
    }

    @IBAction func fromCurrencyTapped(_ sender: Any) {
    }
    @IBAction func toCurrencyTapped(_ sender: Any) {
    }
    @IBAction func convertTapped(_ sender: Any) {
        print("Attempting Conversion",inputTxt.text!)
        let inputDouble = (inputTxt.text! as NSString).doubleValue
        let exchangeRateDouble = (defaultRateLb.text! as NSString).doubleValue
        let result = inputDouble * exchangeRateDouble
        resultLb.text = String(result)
        
    }
    
    func loadDataFromSession() {
        _ = self.view
        print("loading data from session")
        fromCurr = ViewController.session.fromCurrency
        toCurr = ViewController.session.toCurrency
        isCustom = ViewController.session.isCustom
        exchRate = ViewController.session.exchangeRate
        loadImagesAndText(imgView : fromCurrencyImg , txtLb : fromCurrencyLb ,  curr : fromCurr)
        loadExchangeRate()
    }
    
    func loadImagesAndText(imgView : UIImageView , txtLb : UILabel ,  curr : String) {
        loadImg( imgView : fromCurrencyImg , txtLb : fromCurrencyLb ,  curr : fromCurr ) //load FromImg and Text
        loadImg( imgView : toCurrencyImg , txtLb : toCurrencyLb ,  curr : toCurr ) //load toImg and Text
    }
    
    func loadImg ( imgView : UIImageView , txtLb : UILabel , curr : String ) {
        switch curr {
            case CurrencyEnum.USD.rawValue :
                imgView.image = UIImage(named: "us_flag_new.png")
                txtLb.text = CurrencyEnum.USD.rawValue
                break
            case CurrencyEnum.CDN.rawValue :
                imgView.image = UIImage(named: "canada_flag_new.png")
                txtLb.text = CurrencyEnum.CDN.rawValue
                break
            case CurrencyEnum.EURO.rawValue :
                imgView.image = UIImage(named: "eu_flag.png")
                txtLb.text = CurrencyEnum.EURO.rawValue
                break
            case CurrencyEnum.YUAN.rawValue :
                imgView.image = UIImage(named: "china_flag.png")
                txtLb.text = CurrencyEnum.YUAN.rawValue
                break
            case CurrencyEnum.INR.rawValue :
                imgView.image = UIImage(named: "india_flag.png")
                txtLb.text = CurrencyEnum.INR.rawValue
                break
            default :
                print("DEFAULT REACHED")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "changeFromSegue") {
            let vc = segue.destination as! ChangeCurrency
            vc.type = "from"
        }
        if (segue.identifier == "changeToSegue") {
            let vc = segue.destination as! ChangeCurrency
            vc.type = "to"
        }
    }
    
    func getCustomSession () -> MyCustomSession {
        return ViewController.session
    }
    
    func loadExchangeRate() {
        var ses : MyCustomSession = getCustomSession()
        var cnum = CurrencyEnum.self
        stackExchange.isHidden = false
        btnConvert.isHidden = false
        
        if ( ses.fromCurrency == ses.toCurrency ) {
            stackExchange.isHidden = true
            btnConvert.isHidden = true
            resultLb.text = "Both Currencies Are Same"
        }
        if !ses.isCustom {
            if ses.fromCurrency == cnum.USD.rawValue && ses.toCurrency == cnum.CDN.rawValue { // USD to CDN
                ses.exchangeRate = 1.33 ; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
            }  else if ses.fromCurrency == cnum.USD.rawValue && ses.toCurrency == cnum.INR.rawValue { // USD to INR
                ses.exchangeRate = 72.1; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
            }  else if ses.fromCurrency == cnum.USD.rawValue && ses.toCurrency == cnum.YUAN.rawValue { // USD to YUAN
                ses.exchangeRate = 7.02 ; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
            } else if ses.fromCurrency == cnum.USD.rawValue && ses.toCurrency == cnum.EURO.rawValue { // USD to EURO
                ses.exchangeRate = 0.91 ; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
                
            } else if ses.fromCurrency == cnum.CDN.rawValue && ses.toCurrency == cnum.USD.rawValue { // CDN to USD
                ses.exchangeRate = 0.75 ; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
            } else if ses.fromCurrency == cnum.CDN.rawValue && ses.toCurrency == cnum.INR.rawValue { // CDN to INR
                ses.exchangeRate = 54.3 ; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
            } else if ses.fromCurrency == cnum.CDN.rawValue && ses.toCurrency == cnum.YUAN.rawValue { // CDN to YUAN
                ses.exchangeRate = 5.29 ; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
            } else if ses.fromCurrency == cnum.CDN.rawValue && ses.toCurrency == cnum.EURO.rawValue { // CDN to EURO
                ses.exchangeRate = 0.69 ; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
                
            } else if ses.fromCurrency == cnum.INR.rawValue && ses.toCurrency == cnum.USD.rawValue { // INR to USD
                ses.exchangeRate = 0.014 ; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
            } else if ses.fromCurrency == cnum.INR.rawValue && ses.toCurrency == cnum.CDN.rawValue { // INR to CDN
                ses.exchangeRate = 0.019 ; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
            } else if ses.fromCurrency == cnum.INR.rawValue && ses.toCurrency == cnum.YUAN.rawValue { // INR to YUAN
                ses.exchangeRate = 0.097 ; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
            } else if ses.fromCurrency == cnum.INR.rawValue && ses.toCurrency == cnum.EURO.rawValue { // INR to EURO
                ses.exchangeRate = 0.013 ; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
                
            } else if ses.fromCurrency == cnum.YUAN.rawValue && ses.toCurrency == cnum.USD.rawValue { // YUAN to USD
                ses.exchangeRate = 0.14 ; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
            } else if ses.fromCurrency == cnum.YUAN.rawValue && ses.toCurrency == cnum.CDN.rawValue { // YUAN to CDN
                ses.exchangeRate = 0.19 ; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
            } else if ses.fromCurrency == cnum.YUAN.rawValue && ses.toCurrency == cnum.INR.rawValue { // YUAN to INR
                ses.exchangeRate = 10.27 ; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
            } else if ses.fromCurrency == cnum.YUAN.rawValue && ses.toCurrency == cnum.EURO.rawValue { // YUAN to EURO
                ses.exchangeRate = 0.13 ; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
                
            } else if ses.fromCurrency == cnum.EURO.rawValue && ses.toCurrency == cnum.USD.rawValue { // EURO to USD
                ses.exchangeRate = 1.1 ; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
            } else if ses.fromCurrency == cnum.EURO.rawValue && ses.toCurrency == cnum.CDN.rawValue { // EURO to CDN
                ses.exchangeRate = 1.46 ; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
            } else if ses.fromCurrency == cnum.EURO.rawValue && ses.toCurrency == cnum.INR.rawValue { // EURO to INR
                ses.exchangeRate = 79.28 ; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
            } else if ses.fromCurrency == cnum.EURO.rawValue && ses.toCurrency == cnum.EURO.rawValue { // EURO to YUAN
                ses.exchangeRate = 7.72 ; exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
            }
        } else {
            exchRate = ses.exchangeRate ; defaultRateLb.text = String(ses.exchangeRate)
        }
    }
    
}
extension UIViewController {
    func hideKeyboardOnBackgroundTap() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
