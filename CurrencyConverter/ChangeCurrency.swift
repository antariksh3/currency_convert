//
//  ChangeCurrency.swift
//  CurrencyConverter
//
//  Created by JASI on 14/11/19.
//  Copyright © 2019 lambton. All rights reserved.
//

import Foundation
import UIKit

class ChangeCurrency : UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var currencyTable: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.getCurrencies().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = currencyTable.dequeueReusableCell(withIdentifier: "CurrencyTableViewCell") as? CurrencyTableViewCell {
            let newCurrency = DataService.instance.getCurrencies()[indexPath.row]
            cell.updateView(currency: newCurrency)
            return cell
        } else {
            return CurrencyTableViewCell	()
        }
    }
    
    
    @IBOutlet weak var changeLb: UILabel!
    
    var type = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currencyTable.delegate = self
        currencyTable.dataSource = self
        if ( type == "from" ) {
            changeLb.text = "From Currency"
        } else {
            changeLb.text = "To Currency"
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cur = DataService.instance.getCurrencies()[indexPath.row]
        var vc : ViewController = ViewController()
        var mySession = vc.getCustomSession()
        mySession.isCustom = false
        if type == "from" {
            mySession.fromCurrency = cur.currencyName
            self.navigationController?.popViewController(animated: true)
            //self.dismiss(animated: true, completion: nil)
        } else {
            mySession.toCurrency = cur.currencyName
            self.navigationController?.popViewController(animated: true)
            //self.dismiss(animated: true, completion: nil)
        }
        print(type,cur.currencyName)
        //vc.loadDataFromSession()
    }
    
}
